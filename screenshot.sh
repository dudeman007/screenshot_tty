#!/bin/bash
$File_Destination
$current_date
$png_screenshot


File_Destination=/home/jordan/Pictures/Screenshots/;

# Get the current date
current_date=$(date +%d_%m_%Y|sed -e 's/^ *//');

# Set it as png and name it with current date
png_screenshot="console-screenshot-$current_date.png";

snapscreenshot -c1 -x1 > ~/console-screenshot.tga && convert ~/console-screenshot.tga $png_screenshot;

echo "Taking a screenshot of TTY1 and saving as PNG";

if [ -f /home/jordan/$png_screenshot ];
then
    mv /home/jordan/$png_screenshot $File_Destination;
    elif [ -f $File_Destination$png_screenshot ];
    then
        echo "$File_Destination$png_screenshot";
    fi

    if [ -f /home/jordan/console-screenshot.tga ];
    then
        rm /home/jordan/console-screenshot.tga;
        echo "Finished cleaning up my mess.";
    else
        echo "No console-screenshot.tga file to remove!";
    fi

    echo "Success!";

else
    echo "Sorry, the operation FAILED";
fi
