# Screenshot_TTY

A simple bash sript that can be called to take a screenshot inside a TTY for Linux.

This script requires snapscreenshot, which is a old program that isn't in any repos and which must be compiled. 
**Updated: the compressed Snapscreenshot is provided in this repo, simply unpack and ./configure && make/install**
File paths are hardcoded, so you would need to adapt to your chosen file paths.


